package com.virtualpairprogrammers.employeemanagement;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@WebService(name="EmployeeManagementWebService")
public class EmployeeManagementWebServiceImplementation {
	
	@Inject
	private EmployeeManagementServiceLocal service;
	
	public Employee getEmployeeById (int id ) {
		return service.getEmployeeById(id);
	}
	
	public List<Employee> getAllEmployees () {
		
//		Min �ndring
		return service.getAllEmployees();

	}
	
	//Name ovan �r optional f�r att ge klienten ett mer "v�nligt" namn att kontakta. 
	//Anledningen till att vi skapar webservice klassen (ist�llet f�r att 
//	ge den tidigare implementationsklassen @Web annotationen) �r p.g.a. buggar i Glassfish 

}
