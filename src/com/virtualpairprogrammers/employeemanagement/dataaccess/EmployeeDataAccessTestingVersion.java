package com.virtualpairprogrammers.employeemanagement.dataaccess;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@Alternative
public class EmployeeDataAccessTestingVersion implements EmployeeDataAccess {

	@Override
	public void insert(Employee employee) {
	}

	@Override
	public List<Employee> findAll() {
		Employee e1 = new Employee("Bob", "Dowbrain", "Janitor", 40000);
		Employee e2 = new Employee("Liev", "Hamsterdance", "Hacker", 23000);
		Employee e3 = new Employee("Mimmi", "Pigg", "Deckare", 23000);
		Employee e4 = new Employee("Luke", "Skywalker", "Jedi", 99000);
		Employee e5 = new Employee("Anakin", "Skywalker", "Dark Lord", 500000);
		Employee e6 = new Employee("Pingu", "Pingvin", "St�dare", 30);
		
		
		List<Employee> list = new ArrayList<>();
		
		list.add(e1); list.add(e2); list.add(e3); list.add(e4); list.add(e5); list.add(e6);
		
		return list;
		
	}

	@Override
	public List<Employee> findBySurname(String surname) {
		return null;
	}

	@Override
	public Employee findEmployeeById(int id) {
		return null;
	}

}
