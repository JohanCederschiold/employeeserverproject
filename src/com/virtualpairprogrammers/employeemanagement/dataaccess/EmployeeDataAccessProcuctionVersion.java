package com.virtualpairprogrammers.employeemanagement.dataaccess;

import java.util.List;

import javax.ejb.Stateless;
import javax.enterprise.inject.Default;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.virtualpairprogrammers.employeemanagement.domain.Employee;

@Stateless
@Default
public class EmployeeDataAccessProcuctionVersion implements EmployeeDataAccess {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public void insert(Employee employee) {
		
//		Query query = em.createQuery("insert into Employee values "")
		em.persist(employee);
		
	}

	@Override
	public List<Employee> findAll() {
//		Employee e1 = new Employee("Wallee", "LiftLoader", "Janitor", 40000);
//		Employee e2 = new Employee("Jason", "Vorhees", "Slasher", 23000);
//		Employee e3 = new Employee("Batman", "Batman", "Deckare", 23000);
//		Employee e4 = new Employee("Luke", "Skywalker", "Jedi", 99000);
//		Employee e5 = new Employee("Anakin", "Skywalker", "Dark Lord", 500000);
//		Employee e6 = new Employee("Pingu", "Pingvin", "St�dare", 30);
		
		Query query = em.createQuery("select employee from Employee employee");
		
		List<Employee> list = query.getResultList();
				
		return list;
	}

	@Override
	public List<Employee> findBySurname(String surname) {
		
		Query query = em.createQuery("select employee from Employee employee where surname = :surname");
		query.setParameter("surname", surname);
		
		List<Employee> list = query.getResultList();
		
		
		
		return list;
	}

	@Override
	public Employee findEmployeeById(int employeeid) {
		
//		Min �ndring
		
		Query query = em.createQuery("select employee from Employee employee where id = :id");
		query.setParameter("id", employeeid);
//		List<Employee> list = query.getResultList();
//		
//		return list.get(0);
		
		return (Employee) query.getSingleResult();
		
	}

}
